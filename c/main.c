#include <stdio.h>
#include <string.h> /* memset */
#include <libgen.h> /* POSIX basename */
#include <alloca.h>
#include "gs.h"
#include "settings.c"

#define min(X, Y) (((X) < (Y)) ? (X) : (Y))

#define SIGIL_START "{{"
#define SIGIL_END   "}}"
#define EMPTY_STRING ""

typedef struct buffer {
        char *Data;
        unsigned int Size;
} buffer;

config GConfig;

buffer
Lookup(gs_hash_map *Env, buffer Stream)
{
        char TempString[255] = { 0 };
        void *HashResult;
        buffer Result;

        GSStringCopyWithNull(Stream.Data, TempString, Stream.Size);
        if(GSHashMapGet(Env, TempString, &HashResult))
        {
                Result.Data = (char *)HashResult;
                Result.Size = GSStringLength((char *)HashResult);
        }
        else
        {
                Result.Data = NULL;
                Result.Size = 0;
        }

        return(Result);
}

buffer
Trim(buffer Stream)
{
        char *Head = Stream.Data + GSStringLength(SIGIL_START);
        char *Tail = Stream.Data + Stream.Size - GSStringLength(SIGIL_END);
        for(; *Head == ' '; ++Head);
        for(; *Tail == ' '; --Tail); /* Tail now points to last character. */
        buffer Result;
        Result.Data = Head;
        Result.Size = (Tail+1) - Head;
        return(Result);
}

void
Usage(char *ProgramName)
{
        printf("Usage: %s [env pairs] text\n", basename(ProgramName));
        printf("eg.: %s price \\$5 \"The toy costs {{ price }}.\"\n", basename(ProgramName));
        exit(EXIT_SUCCESS);
}

int
main(int ArgCount, char *Arguments[])
{
        gs_args Args;
        GSArgInit(&Args, ArgCount, Arguments);

        ConfigInit(&GConfig);
        if((ArgCount - 2) % 2 != 0 || GSArgHelpWanted(&Args))
        {
                Usage(Arguments[0]);
        }

        char *ConfigFile = "settings.cfg";
        if(GSArgIsPresent(&Args, "--config"))
                ConfigFile = GSArgAfter(&Args, "--config");

        int LargestStringSize = 0;
        gs_hash_map *Env = GSHashMapCreate(-1, NULL);
        for(int i=1; i < ArgCount-1; i+=2)
        {
                GSHashMapAdd(Env, Arguments[i], (void *)Arguments[i+1]);

                int Len = GSStringLength(Arguments[i+1]);
                if(Len > LargestStringSize) LargestStringSize = Len;
        }

        char *ReplaceStart, *ReplaceEnd, *Stream;
        Stream = Arguments[ArgCount-1];

        int NumReplacements = 0;
        char *Cursor = strstr(Stream, SIGIL_START);
        while(Cursor != NULL)
        {
                Cursor = strstr(Cursor + GSStringLength(SIGIL_START), SIGIL_START);
        }


        int SizeToAlloc = NumReplacements * LargestStringSize + GSStringLength(Stream);
        char *Memory = (char *)calloc(1, SizeToAlloc * 2);
        char *FrontBuffer = Memory;
        char *BackBuffer = Memory + SizeToAlloc;
        char *Swap;

        while(true)
        {
                buffer ToReplace, Trimmed, Replaced;
                ReplaceStart = strstr(Stream, SIGIL_START);
                ReplaceEnd = strstr(Stream, SIGIL_END);

                if(ReplaceStart == NULL || ReplaceEnd == NULL) break;
                ReplaceEnd += GSStringLength(SIGIL_END);

                ToReplace.Data = ReplaceStart;
                ToReplace.Size = ReplaceEnd - ReplaceStart - 1;
                Trimmed = Trim(ToReplace);

                char ConfigValue[512] = { 0 };
                char TmpString[512] = { 0 };
                GSStringCopy(Trimmed.Data, TmpString, Trimmed.Size);

                Replaced = Lookup(Env, Trimmed);
                if(Replaced.Size == 0)
                {
                        char *Tmp;
                        Tmp = (char *)alloca(Trimmed.Size);
                        GSStringCopyWithNull(Trimmed.Data, Tmp, Trimmed.Size);
                        Replaced.Data = ConfigGet(&GConfig, Tmp);
                        if(Replaced.Data != NULL)
                                Replaced.Size = GSStringLength(Replaced.Data);
                }

                GSStringCopy(BackBuffer, FrontBuffer, GSStringLength(BackBuffer));
                GSStringCopy(Stream, FrontBuffer + GSStringLength(FrontBuffer), ReplaceStart - Stream);
                GSStringCopy(Replaced.Data, FrontBuffer + GSStringLength(FrontBuffer), Replaced.Size);

                Swap = BackBuffer;
                BackBuffer = FrontBuffer;
                FrontBuffer = Swap;

                Stream = ReplaceEnd;
        }

        GSStringCopy(BackBuffer, FrontBuffer, GSStringLength(BackBuffer));
        GSStringCopy(Stream, FrontBuffer + GSStringLength(FrontBuffer), GSStringLength(Stream));

        printf("%s\n", FrontBuffer);

        return(0);
}
